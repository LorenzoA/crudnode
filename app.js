
const express = require('express')
const server = express()

const hostname = 'localhost'
const port = 3000

server.get('/', (req, res)=> res.send('Welcome on landing page'))

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`)
})